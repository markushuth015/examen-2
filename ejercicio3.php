<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <?php
    function random($length = 4)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }


    $array = array();

    $i = 1;

    while ($i <= 500) {
        $array[$i] = random();
        $i++;
    }
    echo "<table>";
    echo "<tr>";
    echo "<td>Indice</td>";
    echo "<td>Valor</td>";


    foreach ($array as $indice => $valor) {
        echo "<tr>";
        echo "<td>$indice</td>";
        echo "<td>$valor</td>";
        echo "</tr>";
    }

    echo "</table>";
    ?>
</body>

</html>